import { Component, OnInit } from '@angular/core';
import { Post } from '../shared/post.model';
import { PostService } from '../shared/post.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  id: string = '';
  title: string= '';
  body: string = '';
  dateCreated: string = '';

  constructor(private postService: PostService,
              private http: HttpClient) { }

  ngOnInit(): void {
  }
  createPost() {
    const id = this.id;
    const title = this.title;
    const body = this.body;
    const dateCreated = this.dateCreated;
    const post = {title, dateCreated, body};
    this.http.post('https://aidai-fa920-default-rtdb.firebaseio.com/posts.json', post).subscribe();
  }
}
