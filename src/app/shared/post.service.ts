import { Post} from './post.model';
import { EventEmitter } from '@angular/core';

export class PostService {
  postsChange = new EventEmitter<Post[]>();
  posts: Post[] = [
    new Post( 'post1','My life journey', 'Dec 9, 2021', 'Hey hey hey hey hey hey' ),
    new Post( 'post1', 'What I eat and read', 'Dec 9, 2021', 'Hey hey hey hey hey hey' ),
  ];
  getPosts(){
    return this.posts.slice();
  }
  getPost(index: number) {
    return this.posts[index];
  }

  addPost(post: Post) {
    this.posts.push(post);
    this.postsChange.emit(this.posts);
  }
}
