import { Component, OnInit } from '@angular/core';
import { Post } from '../shared/post.model';
import { PostService } from '../shared/post.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts!: Post[];

  constructor(private postService: PostService,
              private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get<{ [id: string]: Post }>('https://aidai-fa920-default-rtdb.firebaseio.com/posts.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const postData = result[id];
          const post = new Post(
            id,
            postData.title,
            postData.dateCreated,
            postData.body
          );
          return post;
        });
      }))
      .subscribe(posts => {
        this.posts = posts;
      });
  }
}
