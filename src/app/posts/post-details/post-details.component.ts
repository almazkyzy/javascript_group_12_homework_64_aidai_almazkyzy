import { Component, OnInit } from '@angular/core';
import { Post } from '../../shared/post.model';
import { PostService } from '../../shared/post.service';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post!: Post;

  constructor(
    private postService: PostService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const postId: number = parseInt(params['id']);
      this.post = this.postService.getPost(postId);
    });
  }
  }


